import requests
import json
from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + state, "per_page": 1}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}

def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    url_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "q": {city, state}
    }
    response = requests.get(url, params=url_params)
    local_data = json.loads(response.content)
    local = {
        "lat": local_data[0]["lat"],
        "lon": local_data[0]["lon"],
    }

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": local["lat"],
        "lon": local["lon"],
    }
    weather_response = requests.get(weather_url, params=weather_params)
    local_weather = json.loads(weather_response.content)
    weather = {
        "temp": local_weather["main"]["temp"],
        "weather": local_weather["weather"],
    }
    return weather

